# Mimir

Mimir is a file indexing platform. It extracts valuable information from your files and allows you to search and organize them.
You can find more information about Mimir on [gomimir.gitlab.io](https://gomimir.gitlab.io).

This repository contains quick start using docker-compose.

## 1. Starting up

Everything is prepared for you to start just with a single command. You can tweak whatever things you like later in the process.

```bash
docker-compose up -d
```

## 2. Your files

Mimir offers 2 strategies how to handle your files.

1. If you prefer to organize your files yourself, copy them into `./data/documents-sorted` (or `./data/photos-sorted`). In that case Mimir is going to keep everything untouched and it is just going to index them. You will have a full control over your files that way.
2. If you prefer Mimir to do all the hard work for you, upload them through the web interface. Mimir is going to place them into auto-generated directories and automatically handle renames, deletions, etc.

You can mix those two however you see fit :)

## 3. Web interface

Your web interface is available on http://127.0.0.1:4050 by default. Visit it and create your first index.
