#!/bin/bash

DIRS=""

while IFS= read -r -d '' DIR; do
  DIRS="$DIRS $DIR"
done < <(find data -not -iname '*-sorted' -type d -depth 1 -print0)

if [ "$DIRS" == "" ]; then
  echo "Nothing to delete"
else
  echo "Following directories will be deleted:"
  echo

  for DIR in $DIRS; do
    echo "- $DIR"
  done

  echo
  read -p "Are you sure? [yN] " -n 1 -r
  echo
fi

docker-compose down -v

if [[ $REPLY =~ ^[Yy]$ ]]; then
  for DIR in $DIRS; do
    echo "Deleting $DIR"
    rm -rf "$DIR"
  done
fi

echo
